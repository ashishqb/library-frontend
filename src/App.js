import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";

import Header from "./components/Header";
import LibraryList from "./components/LibraryList";
import SearchList from "./components/SearchList";
import ReturnForm from "./components/ReturnForm";
import LendedList from "./components/LendedList";
import SupplyForm from "./components/SupplyForm";
import AddBookForm from "./components/AddBookForm";
import GenericNotFound from "./components/GenericNotFound";

import ReduxComponent from "./components/redux/ReduxComponent";

import store from "./store";

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <>
            <Header />
            <Switch>
              <Route exact path="/" component={LibraryList} />
              <Route path="/search" component={SearchList} />
              <Route path="/lended" component={LendedList} />
              <Route path="/supply" component={SupplyForm} />
              <Route path="/return" component={ReturnForm} />
              <Route path="/addbook" component={AddBookForm} />
              <Route path="/redux" component={ReduxComponent} />
              <Route component={GenericNotFound} />
            </Switch>
          </>
        </Router>
      </Provider>
    );
  }
}

export default App;
