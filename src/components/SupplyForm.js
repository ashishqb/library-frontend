import React, { Component } from "react";
import axios from "axios";

export class SupplyForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      error: null,
      items: [],
      selected: 0
    };
  }

  async fetchData() {
    try {
      let stocks = await (await fetch("http://localhost:8080/stock")).json();

      this.setState({
        loaded: true,
        items: stocks.data
      });
    } catch (error) {
      alert("Something went wrong");
    }
  }

  async postData(id) {
    try {
      await axios.post("http://localhost:8080/tolend", {
        bookId: id
      });

      this.fetchData();
    } catch (error) {
      alert("Something went wrong");
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  handleSelect(e) {
    e.preventDefault();
    this.setState({
      selected: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    const state = this.state;
    this.postData(state.items[state.selected]._id);
  }

  render() {
    const stockBooks = this.state.items;

    const options = stockBooks.map((book, index) => {
      return (
        <option key={index} value={index}>
          {book.name}
        </option>
      );
    });

    if (stockBooks.length) {
      return (
        <>
          <form onSubmit={e => this.handleSubmit(e)}>
            <label>
              Choose your book to lend
              <br />
              <br />
              <select onChange={event => this.handleSelect(event)}>
                {options}
              </select>
              <br />
              <br />
              <input type="submit" value="Submit" />
            </label>
          </form>
        </>
      );
    } else {
      return <div>No books!</div>;
    }
  }
}

export default SupplyForm;
