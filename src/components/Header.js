import React, { Component } from "react";
import { Link } from "react-router-dom";

export class Header extends Component {
  render() {
    return (
      <header style={headerStyle}>
        <h2>Library Management</h2>
        <Link style={linkStyle} to="/">
          Stock
        </Link>{" "}
        |{" "}
        <Link style={linkStyle} to="/search">
          Search
        </Link>{" "}
        |{" "}
        <Link style={linkStyle} to="/lended">
          Lended
        </Link>{" "}
        |{" "}
        <Link style={linkStyle} to="/supply">
          Supply
        </Link>{" "}
        |{" "}
        <Link style={linkStyle} to="/return">
          Return
        </Link>{" "}
        |{" "}
        <Link style={linkStyle} to="/addbook">
          Add Book
        </Link>{" "}
        |{" "}
        <Link style={linkStyle} to="/redux">
          Redux
        </Link>
      </header>
    );
  }
}
const headerStyle = {
  background: "#333",
  textAlign: "center",
  padding: "10px"
};

const linkStyle = {
  color: "#fff"
};

export default Header;
