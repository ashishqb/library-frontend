import React, { Component } from "react";
import axios from "axios";

function SearchData(props) {
  return (
    <ul>
      {props.data.map(item => (
        <li key={item._id}>
          {item.name}{" "}
          <b>
            <i>{item.lended ? "Lended" : "In Stock"}</i>
          </b>
        </li>
      ))}
    </ul>
  );
}

export class SearchList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      search: false,
      searchData: []
    };
  }

  handleChange(event) {
    this.setState({
      name: event.target.value
    });
  }

  async handleSubmit(event) {
    event.preventDefault();
    let search = await axios.get("http://localhost:8080/search", {
      params: {
        name: this.state.name
      }
    });

    this.setState({
      search: true,
      searchData: search.data.data
    });
  }

  render() {
    return (
      <div>
        <div>
          <form onSubmit={event => this.handleSubmit(event)}>
            <br />
            <input
              type="text"
              value={this.state.name}
              placeholder="Enter the name to search"
              onChange={event => this.handleChange(event)}
            />
            <br />

            <button type="submit">Search</button>
          </form>
        </div>
        <div>
          {this.state.search ? (
            <SearchData data={this.state.searchData} />
          ) : null}
        </div>
      </div>
    );
  }
}

export default SearchList;
