import React, { Component } from "react";
import axios from "axios";

export class AddBookForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      book: "",
      author: ""
    };
  }

  async postData(body) {
    try {
      await axios.post("http://localhost:8080/addbook", body);
      this.setState({
        book: "",
        author: ""
      });
    } catch (error) {
      alert("Error in adding book");
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    this.postData(this.state);
  }

  handleBookChange(e) {
    e.preventDefault();
    this.setState({
      book: e.target.value
    });
  }

  handleAuthorChange(e) {
    e.preventDefault();
    this.setState({
      author: e.target.value
    });
  }

  render() {
    return (
      <>
        <form onSubmit={e => this.handleSubmit(e)}>
          <label>Book name</label>
          <br />
          <input
            type="text"
            placeholder="Book name"
            value={this.state.book}
            onChange={event => {
              this.handleBookChange(event);
            }}
          />
          <br />
          <label>Author</label>
          <br />
          <input
            type="text"
            placeholder="Author name"
            value={this.state.author}
            onChange={event => this.handleAuthorChange(event)}
          />
          <br />
          <br />
          <input type="submit" value="Submit" />
        </form>
      </>
    );
  }
}

export default AddBookForm;
