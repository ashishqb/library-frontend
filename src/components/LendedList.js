import React, { Component } from "react";

export class LendedList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      error: null,
      items: []
    };
  }

  async fetchData() {
    let stocks = await (await fetch("http://localhost:8080/lended")).json();

    this.setState({
      loaded: true,
      items: stocks.data
    });
  }

  componentDidMount() {
    this.fetchData();
  }

  render() {
    const { error, loaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!loaded) {
      return <div>Loading...</div>;
    } else if (!items.length) {
      return <div>No books!</div>;
    } else {
      return (
        <div>
          <ul>
            {items.map(item => (
              <li key={item.name}>
                {item.name}
                <ul>
                  <li> {item.author}</li>
                </ul>
              </li>
            ))}
          </ul>
        </div>
      );
    }
  }
}

export default LendedList;
