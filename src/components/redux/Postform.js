import React, { Component } from "react";
import { connect } from "react-redux";
import { createPost } from "../../actions/postActions";

class Postform extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      body: ""
    };
  }

  handleChange(e) {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  async fetchPost() {
    try {
      const post = {
        title: this.state.title,
        body: this.state.body
      };

      fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify(post)
      });
    } catch (error) {
      alert("Something went wrong");
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    const post = {
      title: this.state.title,
      body: this.state.body
    };
    this.props.createPost(post);
  }

  render() {
    return (
      <div>
        <h3>Add Post</h3>
        <form onSubmit={event => this.handleSubmit(event)}>
          <div>
            <label>Title</label>
            <br />
            <input
              type="text"
              name="title"
              value={this.state.title}
              onChange={event => this.handleChange(event)}
            />
          </div>
          <div>
            <label>Body</label>
            <br />
            <textarea
              name="body"
              value={this.state.body}
              onChange={event => this.handleChange(event)}
            />
          </div>
          <br />
          <button type="submit">Submit</button>
        </form>
      </div>
    );
  }
}

export default connect(
  null,
  { createPost }
)(Postform);
