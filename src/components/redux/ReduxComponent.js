import React, { Component } from "react";

import Posts from "./Posts";
import Postform from "./Postform";

export class ReduxComponent extends Component {
  render() {
    return (
      <>
        <Postform />
        <hr />
        <Posts />
      </>
    );
  }
}

export default ReduxComponent;
