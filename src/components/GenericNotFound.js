import React, { Component } from "react";

export class GenericNotFound extends Component {
  render() {
    return (
      <div>
        <h3>404 route not found</h3>
      </div>
    );
  }
}

export default GenericNotFound;
